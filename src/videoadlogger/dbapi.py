#!/usr/bin/python
# -*- coding: utf-8 -*-
import MySQLdb as driver
import warnings
import re
from datetime import datetime

from videoadlogger import AdLoggerError, dateformat

adregistratedFields     = ['id', 'service_name', 'adv_link', 'video_link', 'client', 'stamp',
                           'content_type', 'content_name', 'content_id']

fields = dict(
    adregistrated = adregistratedFields,
)

#тестирование производительности запросов к субд
import time
def timer(f):
    def tmp(*args, **kwargs):
        print '-------start test---------'
        t = time.time()
        res = f(*args, **kwargs)
        print 'Run time function: %f' % (time.time()-t)
        return res
    return tmp

class DbApi(object):
    __dbLog = None
    __logger = None
    __config = None
    #database auth parameters
    __hostname = None
    __dbname = None
    __username = None
    __password = None
    #status dbapi
    __status = None

    def __init__(self, logger, config):
        self.__logger = logger
        self.__logger.info('Init db...')
        self.__config = config

        try:
            self.__hostname = self.__config.get('database').get('hostname')
            self.__dbname = self.__config.get('database').get('dbname')
            self.__username = self.__config.get('database').get('username')
            self.__password = self.__config.get('database').get('password')
        except:
            message = 'Config parse for database auth parameters failed! Check hostname, dbname, username and password'
            self.__logger.critical(message)
            raise AdLoggerError(message)

        self.__queryLimit = config.get('database',dict()).get('querylimit', 50)
        try:
            dbtype = self.__config.get('database').get('type')
        except:
            message = 'Config parse for database type error!'
            self.__logger.critical(message)
            raise AdLoggerError(message)

        try:
            if dbtype != 'mysql':
                message = 'Incorrect db type into config'
                self.__logger.critical(message)
                raise AdLoggerError(message)
        except AdLoggerError, ex:
            raise ex

        self.__status = 'work'

        self.__create_structure()
        self.__logger.info('Init db done.')

    def __create_structure(self):
        cursor = self.__get_connection()

        warnings.filterwarnings('ignore')  #ignore warnings  like  Warning: Table 'films' already exists

        cursor.execute('set names utf8;')

        cursor.execute("create table if not exists adregister("
                       "id integer auto_increment primary key, "
                       "service_name varchar(25) ,"
                       "adv_link text, "
                       "video_link text, "
                       "client text, "
                       "stamp datetime, "
                       "content_type tinyint not null, "
                       "content_name varchar(400) default '', "
                       "content_id integer default 0, "
                       "KEY i_sn (service_name) "
                       ") character set utf8 collate utf8_general_ci;")

        cursor.execute("create table if not exists systems ("
                       "setting text not null, "
                       "value integer not null "
                       ") character set utf8 collate utf8_general_ci;")

        db_version = 20121204
        def dbVersion(version):
            get_version = self.__get("select value from systems where setting='dbversion';")
            if not get_version: #first run or version update
                self.__get('insert into systems(setting, value) values (%s, %s);',('dbversion', version))
                get_version = self.__get("select value from systems where setting='dbversion';")
            return get_version[0]
        (version, ) = dbVersion(db_version)

        #patch mechanism realization
        if version<db_version:
            cursor.execute("delete from systems where setting='dbversion';")
            cursor.execute("alter table adregister modify column service_name varchar(25);") #modify service_name type
            cursor.execute("alter table adregister add index i_sn (service_name);") #add service name index
            cursor.execute("alter table adregister modify column stamp datetime;") #modify service_name type
            cursor.execute("alter table adregister add column content_type tinyint not null;") #add type of content adv(0)/video(1)
            cursor.execute("alter table adregister add column content_name varchar(400) default '';") #add content name
            cursor.execute("alter table adregister add column content_id integer default 0;") #add content id
            (version, ) = dbVersion(db_version)

        try:
            self.__close_connection(cursor)
        except Exception, ex:
            message = ('Creating structure failed! Reason: %s' % str(ex))
            self.__logger.critical(message)
            raise AdLoggerError(message)

        self.__dbVersion = version

        return

    def reconnect(self):
        if not self.__status:
            raise AdLoggerError('Database interface not prepared or configured')
        self.__dbLog = driver.connect(
            host=self.__hostname,
            user=self.__username,
            passwd=self.__password,
            db=self.__dbname
        )
        self.__dbLog.set_character_set('utf8')
        return self.__dbLog

    def __get_connection(self):
        self.reconnect()
        cursor = self.__dbLog.cursor()
        return cursor

    def __close_connection(self, cursor):
        try:
            self.__dbLog.commit()
        except Exception, ex:
            self.__dbLog.rollback()
            raise Exception('Failed commit action. Reason: %s' % str(ex))
        finally:
            cursor.close()
        return True

#    @timer
    def __get(self, request, params=dict()):
        if not request:
            return None

        try:
            cursor = self.__get_connection()
            if params:
                cursor.execute(request, params)
            else:
                cursor.execute(request)

            result = None
            try:
                result = cursor.fetchall()
            finally:
                self.__close_connection(cursor)
                return result
        except Exception, ex:
            if re.search('MySQL server has gone away', str(ex)):
                self.__logger.critical('Database connection was closed. Trying reconnect and reexecute query...')
                self.reconnect()
                return self.__get(request, params)
            else:
                message = ('Failed by executing query [%s].Reason %s' % (request, str(ex)))
                self.__logger.critical(message)
                raise AdLoggerError(message)

    def getDbVersion(self):
        get_version = self.__get("select value from systems where setting='dbversion';")
        (self.__dbVersion, ) = get_version[0]
        return self.__dbVersion

    #db-api
    def getIds(self, table):
        '''gets ids from sets table by parameter'''
        ids = self.__get('select id from ' + table + ';')
        return map(lambda id: id[0], ids)

    #queries for user-api
    def registerAdReview(self, params):
        params['stamp'] = datetime.now().strftime(dateformat)
        for k in params.keys():
            if (not k in adregistratedFields) or k=='id':
                del params[k]
        query = ('insert into adregister(service_name, adv_link, video_link, client, stamp, content_type, content_name, content_id) '
                 'values(%s, %s, %s, %s, %s, %s, %s, %s);')
        qparams = (
            params.get('service_name'),
            params.get('adv_link'),
            params.get('video_link'),
            params.get('client'),
            params.get('stamp'),
            params.get('content_type'),
            params.get('content_name'),
            params.get('content_id'),
            )
        try:
            self.__get(query, qparams)
            return dict(result = 'Ok')
        except:
            return dict(result = 'Failed')
