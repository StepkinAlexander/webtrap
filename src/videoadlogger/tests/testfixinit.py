# -*- coding: utf-8 -*-
import unittest
from videoadlogger.tests import fixtures
from unittest import TestCase

fixtures.log_init()
fixtures.dbapi_init()

class TF(TestCase):
    def setUp(self):
        fixtures.all_to_default()

    def tearDown(self):
        pass

class TestParsing(TF):
    def testDefaultData(self):
        self.assertEqual(len(fixtures.dbapi.getIds('adregister')), 1, 'Not awaiting entries quantity in table "adregister"!')

if __name__=='__main__':
    unittest.main()
