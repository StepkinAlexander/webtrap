#!/bin/sh
export PYTHONPATH=../../

echo "====starting tests===="
echo "1. testing fixtures init"
`which python` ./testfixinit.py

echo "2. testing dbapi(responsed fields)"
`which python` ./testdbapi.py

echo "3. testing userapi(responsed quantity results)"
`which python` ./testuserapi.py
echo "====tests done===="
