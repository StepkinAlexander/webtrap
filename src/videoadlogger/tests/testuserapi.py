# -*- coding: utf-8 -*-
import unittest
from unittest import TestCase

import json
from videoadlogger.tests import fixtures
from videoadlogger.tests.common import Request
from videoadlogger.userapi import UserApi

fixtures.log_init()
fixtures.dbapi_init()

class TUA(TestCase):
    def setUp(self):
        fixtures.all_to_default()
        self.userapi = UserApi(fixtures.log, fixtures.dbapi)

    def tearDown(self):
        del self.userapi


class TestRegister(TUA):
    def test1(self):
        _expected_ = 'Ok'
        request = Request(u'register/service_name=ivi&adv_link=some.link&video_link=other.some.link&client=me&content_type=0&content_name=&content_id=10')

        res = json.loads(self.userapi.render_GET(request)).get('result')
        self.assertEqual(res, _expected_, 'Result different than expected')


if __name__=='__main__':
    unittest.main()
