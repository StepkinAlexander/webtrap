#!/usr/bin/python
# -*- coding: utf-8 -*-

config = {
    "logdest": "stdout",
    "statusdest": "",
    "database": {
        "type": "mysql",
        "querylimit": 5,
        "username": "root",
        "password": "q1q1q1",
        "hostname": "localhost",
        "dbname": "adlogtest"
    },
    "userapi": {
        "port": 8084
    }
}
