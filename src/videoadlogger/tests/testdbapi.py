# -*- coding: utf-8 -*-
import unittest
from unittest import TestCase

import json
from videoadlogger.tests import fixtures
from videoadlogger.tests.common import Request
from videoadlogger.userapi import UserApi

fixtures.log_init()
fixtures.dbapi_init()

class TDA(TestCase):
    def setUp(self):
        fixtures.all_to_default()
        self.userapi = UserApi(fixtures.log, fixtures.dbapi)

    def tearDown(self):
        del self.userapi


#tests for checking results format
class TestDbapi(TDA): #request: /series/video=*
    def testCommon(self):
        self.skipTest('Not implement need test methods')

if __name__=='__main__':
    unittest.main()
