# -*- coding: utf-8 -*-
from videoadlogger.tests import config
from videoadlogger.dbapi import DbApi
from videoadlogger.tests.common import FakeLogger

log = None
dbapi = None

import MySQLdb as driver
db = driver.connect(
    host=config.get('database').get('hostname'),
    user=config.get('database').get('username'),
    passwd=config.get('database').get('password'),
    db=config.get('database').get('dbname')
)
db.set_character_set('utf8')

import json

def log_init():
    global log
    log = FakeLogger()

def dbapi_init():
    #dbapi init
    global dbapi
    dbapi = DbApi(log, config)

def fake_app_finish():
    #imitate start application and parse xml once
    log_init()
    dbapi_init()

adregister = {
    '1': dict(
        id=u'3',
        service_name=u'ivi',
        adv_link=u'some.link',
        video_link=u'other.some.link',
        client=u'me',
        stamp=u'2012-11-21 15:10:54',
        content_type=u'0',
        content_name=u'',
        content_id=u'10',
    ),
}

tables = {
    'adregister': adregister,
}
def all_to_default():
    def clear_table(table):
        cur = db.cursor()
        cur.execute('delete from %s;' % table)
        db.commit()
        cur.close()

    for t in tables.keys():
        clear_table(t)
        table_to_default(t, tables.get(t))

def table_to_default(table, data):
    cur = db.cursor()
    for d in data.keys():
        elem = data.get(d)
        vls = tuple()
        for k in elem.keys():
            vls = vls + (elem.get(k),)
        query = 'insert into ' + table + '(' + str.join(',', elem.keys()) + ') values (' + str.join(',', map(lambda v: '%s', elem.keys())) + ');'
        cur.execute(query, vls)
    db.commit()
    cur.close()
