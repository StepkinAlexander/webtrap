#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
import signal

#logging
import logging, logging.handlers
log = logging.getLogger('videoadlogger')

#webserver and service core
from twisted.web import server
from twisted.internet import reactor

from videoadlogger import AdLoggerError, config as _config_, saveStatus, dateformat
from videoadlogger.dbapi import DbApi
from videoadlogger.userapi import UserApi

#Service Core
class Web(object):
    process = 'informer'
    def __init__(self):
        pass

    def run(self):
        signal.signal(signal.SIGINT, self.stop)
        signal.signal(signal.SIGHUP, self.stop)
        saveStatus(service='START', app=self.process)
        reactor.run()

    def stop(self, s, p):
        log.info('Stop ' + self.process + ' application[stopsignal: %s]' % s)
        self.__running = False
        saveStatus(service='STOP', app=self.process)
        reactor.stop()

def main(process='informer'):
    #logger initialize
    log.setLevel(logging.INFO)
    log_format = '%(asctime)s %(name)s %(levelname)-8s ' + process + ' | %(message)s'
    formatter = logging.Formatter(log_format, datefmt=dateformat)

    logdest = _config_.get('logdest', 'stdout')
    if logdest=='stdout':
        hdl = logging.StreamHandler()
    elif logdest=='syslog':
        hdl = logging.handlers.SysLogHandler(address = '/dev/log')
    else:
        logdest = logdest[:-1] if logdest[-1:]=='/' else logdest
        if process == 'informer':
            logdest = logdest + '/informer.log'
        else:
            raise AdLoggerError('Unknown process trying starts. Break!')
        hdl = logging.FileHandler(filename=logdest, mode='a')

    hdl.setFormatter(formatter)
    log.addHandler(hdl)

    #configure db fasade
    try:
        dblog = DbApi(log, _config_)
    except AdLoggerError, pe:
        raise AdLoggerError(str(pe))

    log.info('videoadlogger ' + process + ' service starts [database version: %s]...' % dblog.getDbVersion())
    #application runs
    if process == 'informer':
        site = server.Site(UserApi(logger=log, db=dblog))
        reactor.listenTCP(int(_config_.get('userapi', dict()).get('port','8083')), site)
        app = Web()
    app.run()

if __name__ == '__main__':
    main()
