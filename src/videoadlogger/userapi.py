#!/usr/bin/python
# -*- coding: utf-8 -*-
import re
import json
from datetime import datetime

from twisted.web import resource
from urllib import unquote

from videoadlogger import saveStatus, dateformat


class UserApi(resource.Resource):
    isLeaf = True
    __db = None
    __logger = None
    methods = None

    def __init__(self, logger, db):
        resource.Resource.__init__(self)

        self.__logger = logger
        self.__db = db
        self.methods = {
            'register': self.__db.registerAdReview,
            }

    def render_GET(self, request):
        def incorrectCommand(params=None):
            return 'Incorrect request'
        uri = unquote(request.uri[1:]) if request.uri[0:1]=='/' else unquote(request.uri)
        if uri[-1:]=='/': uri = uri[:-1]

        if uri != 'favicon.ico':
            self.__logger.info('GET request: %s (from %s)', uri, request.getClientIP())
            saveStatus(request=uri, requesttime=datetime.now().strftime(dateformat))

            uri = uri.replace("'", '').replace('"', '')
            try:
                command = uri.split('/')
                if not(command[0] in self.methods.keys()):
                    self.__logger.critical('incorrect request: ', uri)
                    return 'Incorrect request'
                params = dict()
                paramsline = uri[len(command[0])+1:]
                try:
                    params['service_name'] = re.findall('service_name=(.+?)&adv_link',paramsline)[0]
                except:
                    params['service_name'] = ''
                try:
                    params['adv_link'] = re.findall('adv_link=(.+?)&video_link',paramsline)[0]
                except:
                    params['adv_link'] = ''
                try:
                    params['video_link'] = re.findall('video_link=(.+?)&client',paramsline)[0]
                except:
                    params['video_link'] = ''
                try:
                    params['client'] = re.findall('client=(.+?)&content_type',paramsline)[0]
                except:
                    params['client'] = ''
                try:
                    params['content_type'] = re.findall('content_type=(.+?)&content_name',paramsline)[0]
                except:
                    params['content_type'] = ''
                try:
                    params['content_name'] = re.findall('content_name=(.+?)&content_id',paramsline)[0]
                except:
                    params['content_name'] = ''
                try:
                #это завершающий параметр, важно, он смотрит
                # от себя и до конца строки в отличие от предыдущих:
                # от себя до следующего
                    params['content_id'] = re.findall('content_id=(.+?)$',paramsline)[0]
                except:
                    params['content_id'] = ''

                if not (params.get('client') or params.get('service_name')):
                    #check for mandatory parameters
                    raise Exception('Not enough mandatory parameters')
            except Exception,  ex:
                self.__logger.critical('Exception ' + str(ex))
                return 'Incorrect request'

            result = json.dumps(self.methods.get(command[0],incorrectCommand)(params))
            saveStatus(answer=result, answertime=datetime.now().strftime(dateformat))
            return result
