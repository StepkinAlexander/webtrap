#!/usr/bin/python
# -*- coding: utf-8 -*-
import json

#app exception
class AdLoggerError(Exception):
    pass

#config
try:
    config = json.load(open('/etc/videoadlogger/config.json', 'r'))
except:
    config = dict()

dateformat = '%Y-%m-%d %H:%M:%S'

#SERVICE STATUS
__status = dict(
    updatestatus = '',
    updatetime = '',
    request = '',
    requesttime = '',
    answer = '',
    answertime = '',
    service = dict(digger='', web=''),
    config = config,
)
def saveStatus(updatestatus = None, updatetime = None,
               request = None, requesttime = None,
               answer = None, answertime = None,
               service = None, app = None):
    if not config.get('statusdest', ''):
        #if not needance
        return
    if updatestatus:
        __status['updatestatus'] = updatestatus
    if updatetime:
        __status['updatetime'] = updatetime
    if request:
        __status['request'] = request
    if requesttime:
        __status['requesttime'] = requesttime
    if answer:
        __status['answer'] = answer
    if answertime:
        __status['answertime'] = answertime
    if app:
        if service:
            __status['service'][app] = service
    w = open(config.get('statusdest'), 'w')
    w.write(json.dumps(__status))
    w.close()
