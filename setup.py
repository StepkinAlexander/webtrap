#!/usr/bin/env python
from distutils.core import setup
dist = setup(
    name='videoadlogger',
    version='1.0',
    description = 'Python modules for video ad logger',
    long_description = 'Python modules(Core) for video ad logger',
    author='Aleksandr Stepkin',
    author_email='stepkin@jetstyle.ru',
    url='',
    license='gpl',

    package_dir={'':'src'},
    packages=['videoadlogger'],

    data_files=[('/etc/videoadlogger', ['install/config.json-default']),
                ('/usr/bin',['install/videoadloggerd'])]
)
